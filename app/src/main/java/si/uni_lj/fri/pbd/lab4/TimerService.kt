package si.uni_lj.fri.pbd.lab4

import android.app.Service
import android.content.Intent
import android.os.IBinder
import android.util.Log

class TimerService : Service() {
    companion object {
        private val TAG: String? = TimerService::class.simpleName
        const val ACTION_STOP = "stop_service"
        const val ACTION_START = "start_service"
        private const val channelID = "background_timer"
        // TODO: define a static final int NOTIFICATION_ID
    }

    // TODO: move startTime, endTime, isTimerRunning from the MainActivity

    // TODO: Define serviceBinder and instantiate it to RunServiceBinder

    override fun onCreate() {
        Log.d(TAG, "Creating service")

        // TODO: set startTime, endTime, isTimerRunning to default values

        // TODO: create notification channel
    }

    override fun onStartCommand(intent: Intent, flags: Int, startId: Int): Int {
        Log.d(TAG, "Starting service")

        // TODO: check the intent action and if equal to ACTION_STOP, stop the foreground service
        return START_STICKY
    }

    override fun onBind(intent: Intent): IBinder? {
        Log.d(TAG, "Binding service")

        // TODO: Return serviceBinder
        return null
    }

    override fun onDestroy() {
        super.onDestroy()
        Log.d(TAG, "Destroying service")
    }

    /**
     * Creates a notification for placing the service into the foreground
     *
     * @return a notification for interacting with the service when in the foreground
     */
    // TODO: Uncomment for creating a notification for the foreground service
    /*  private fun createNotification(): Notification {

        // TODO: add code to define a notification action

        val builder = NotificationCompat.Builder(this, channelID)
                .setContentTitle(getString(R.string.notif_title))
                .setContentText(getString(R.string.notif_text))
                .setSmallIcon(R.mipmap.ic_launcher)
                .setChannelId(channelID)

        val resultIntent = Intent(this, MainActivity::class.java)
        val resultPendingIntent = PendingIntent.getActivity(this, 0, resultIntent,
                PendingIntent.FLAG_UPDATE_CURRENT)
        builder.setContentIntent(resultPendingIntent)
        return builder.build()
    }*/
    // TODO: Uncomment for creating a notification channel for the foreground service
    /* private fun createNotificationChannel() {
        if (Build.VERSION.SDK_INT < 26) {
            return
        } else {
            val channel = NotificationChannel(channelID, getString(R.string.channel_name), NotificationManager.IMPORTANCE_LOW)
            channel.description = getString(R.string.channel_desc)
            channel.enableLights(true)
            channel.lightColor = Color.RED
            channel.enableVibration(true)
            val managerCompat = getSystemService(NOTIFICATION_SERVICE) as NotificationManager
            managerCompat.createNotificationChannel(channel)
        }
    }*/

}