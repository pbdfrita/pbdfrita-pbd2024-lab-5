package si.uni_lj.fri.pbd.lab4

import android.os.Bundle
import android.util.Log
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {
    companion object {
        private val TAG = MainActivity::class.simpleName
        // TODO: Uncomment to get periodic UI updates
        // Message type for the handler
        private const val MSG_UPDATE_TIME = 1
        private const val UPDATE_RATE_MS = 1000L
    }

    // TODO: Create timerService and serviceBound

    // Start and end times in milliseconds
    private var startTime: Long = 0
    private var endTime: Long = 0

    /**
     * @return whether the timer is running
     */
    // Is the service tracking time?
    var isTimerRunning = false
        private set

    // Handler to update the UI every second when the timer is running
    // TODO: Uncomment to get periodic UI updates
    /*private val updateTimeHandler = object : Handler(Looper.getMainLooper()){
        override fun handleMessage(message: Message) {
            if (MSG_UPDATE_TIME == message.what) {
                Log.d(TAG, "updating time")
                updateUITimer()
                sendEmptyMessageDelayed(MSG_UPDATE_TIME, UPDATE_RATE_MS)
            }
        }
    }
    */

    // TODO: Uncomment to define a ServiceConnection
    /* private val mConnection: ServiceConnection = object : ServiceConnection {
        override fun onServiceConnected(componentName: ComponentName, iBinder: IBinder) {
            Log.d(TAG, "Service bound")
            val binder = iBinder as TimerService.RunServiceBinder
            timerService = binder.service
            serviceBound = true
            // Update the UI if the service is already running the timer
            if (timerService?.isTimerRunning == true) {
                updateUIStartRun()
            }
        }

        override fun onServiceDisconnected(componentName: ComponentName) {
            Log.d(TAG, "Service disconnect")
            serviceBound = false
        }
    }*/

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
    }

    override fun onStart() {
        super.onStart()

        // TODO: uncomment this and write the code to the Service
        // Log.d(TAG, "Starting and binding service");

        // TODO: then uncomment this to bind the Service
        // bindService(i, mConnection, 0);
    }

    override fun onStop() {
        super.onStop()
        updateUIStopRun()

        // TODO: if the Service is bound, unbind it
    }

    fun runButtonClick(v: View?) {

        // TODO: modify to check whether the service is bound and whether the service's timer is running
        //  and then start/stop the service's timer
        if (!isTimerRunning) {
            Log.d(TAG, "Starting timer")
            startTimer()
            updateUIStartRun()
        } else {
            Log.d(TAG, "Stopping timer")
            stopTimer()
            updateUIStopRun()
        }
    }

    /**
     * Updates the UI when a run starts
     */
    private fun updateUIStartRun() {
        // TODO: Uncomment to start periodic UI updates
        // updateTimeHandler.sendEmptyMessage(MSG_UPDATE_TIME)
        timer_button.text = getString(R.string.timer_stop_button)
    }

    /**
     * Updates the UI when a run stops
     */
    private fun updateUIStopRun() {
        // TODO: Uncomment to stop periodic UI updates
        // updateTimeHandler.removeMessages(MSG_UPDATE_TIME)
        timer_button.text = getString(R.string.timer_start_button)
    }

    /**
     * Updates the timer readout in the UI; the service must be bound
     */
    private fun updateUITimer() {
        timer_text_view.text = elapsedTime().toString() + " seconds"

        // TODO: check if the Service is bound and set the text view
    }
    // TODO: startTimer, stopTimer, isTimerRunning, elapsedTime will go to the Service
    /**
     * Starts the timer
     */
    fun startTimer() {
        if (!isTimerRunning) {
            startTime = System.currentTimeMillis()
            isTimerRunning = true
        } else {
            Log.e(TAG, "startTimer request for an already running timer")
        }
    }

    /**
     * Stops the timer
     */
    fun stopTimer() {
        if (isTimerRunning) {
            endTime = System.currentTimeMillis()
            isTimerRunning = false
        } else {
            Log.e(TAG, "stopTimer request for a timer that isn't running")
        }
    }

    /**
     * Returns the  elapsed time
     *
     * @return the elapsed time in seconds
     */
    fun elapsedTime(): Long {
        // If the timer is running, the end time will be zero
        return if (endTime > startTime) (endTime - startTime) / 1000 else (System.currentTimeMillis() - startTime) / 1000
    }


}